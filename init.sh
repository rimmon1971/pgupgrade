#!/bin/sh
PGDATA=/var/lib/postgresql/${NEWVERSION}/data

PGDATAOLD=/var/lib/postgresql/${OLDVERSION}/data
PGDATANEW=${PGDATA}

PGBINOLD=/usr/libexec/postgresql${OLDVERSION}
PGBINNEW=/usr/libexec/postgresql${NEWVERSION}

export PGDATA PGDATAOLD PGDATANEW PGBINOLD PGBINNEW

echo "$PGDATAOLD $PGDATANEW"
echo "$OWNER $LC"

chown -R postgres:postgres ${PGDATA}
chmod -R 777 ${PGDATA}
su postgres -c "initdb -U ${OWNER} --locale=${LC}"
echo "listen_addresses = '*'" >> ${PGDATA}/postgresql.conf
echo "host all all all md5" >> ${PGDATA}/pg_hba.conf
su postgres -c "pg_upgrade -v -U ${OWNER}"