#!/bin/bash
source .env
docker build \
 -t ${DOCKERUSER}/pgupgrade:${OLDVERSION}-${NEWVERSION} \
 --build-arg OLDVERSION=${OLDVERSION} \
 --build-arg NEWVERSION=${NEWVERSION} \
 --build-arg OWNER=${OWNER} \
 --build-arg LC=${LC} \
 .

docker push ${DOCKERUSER}/pgupgrade:${OLDVERSION}-${NEWVERSION}