# Docker container to upgrade Calckey's PostgreSQL instance

## Environment file

The **.env** file contains the environment variables needed for customization:

- the Docker Hub user for pushing the image
- The starting version for PostgreSQL
- The desired final version
- The "superuser" for the PostgreSQL instance
- The locale setting

## Build phase (optional)

After having customized the **.env** file, you can launch the **build.sh** script.

Actually, the images 
- **docker.io/rimmon1971/pgupgrade:12-15**
- **docker.io/rimmon1971/pgupgrade:13-15**

are already built and available.

## Stopping Calckey environment

From within the runtime directory of your Calckey instance (the one containing **docker-compose.yml** file as well as **.config** subdir), you should stop the instance:

```
docker-compose down
```

## Preparation for the upgrade

You should copy this entire directory (**pgupgrade**) inside your Calckey instance's runtime directory.

After that, you should enter the copied directory and launch

```
docker-compose up
```

After having checked that the operation has completed without errors, you should cleanup the Docker environment

```
docker-compose down
```

## Editing your Calckey's docker-compose.yml

You should refer to a new PostgreSQL image version, as well as another DB directory to mount.

### Before (example)

```
  db:
    restart: unless-stopped
    image: docker.io/postgres:12.2-alpine
    container_name: calckey_db
    networks:
      - calcnet
    env_file:
      - .config/docker.env
    volumes:
      - ./db:/var/lib/postgresql/data
```

### After (example)

```
  db:
    restart: unless-stopped
#    image: docker.io/postgres:12.2-alpine
    image: docker.io/postgres:15.1-alpine3.17
    container_name: calckey_db
    networks:
      - calcnet
    env_file:
      - .config/docker.env
    volumes:
#      - ./db:/var/lib/postgresql/data
      - ./db2:/var/lib/postgresql/data
```

## Restarting your Calckey instance

From within your runtime directory, you should launch, as usual,

```
docker-compose up -d
```

And monitor your **db**'s and **web**'s container logs:

```
docker-compose logs --follow db
```

```
docker-compose logs --follow web
```

## Follow-up: generate DB statistics

As per **pg_upgrade** suggestion, let's run the appropriate **vacuumdb** command on the new DB container

```
calckey@vmi1146369:~/calckey/run$ docker-compose run --rm db vacuumdb -U calckey -h db --all --analyze-in-stages
```