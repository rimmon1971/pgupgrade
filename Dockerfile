FROM alpine:3.17

ARG OLDVERSION
ARG NEWVERSION
ARG OWNER
ARG LC
ENV OLDVERSION $OLDVERSION
ENV NEWVERSION $NEWVERSION
ENV OWNER $OWNER
ENV LC $LC

# installing latest PostgreSQL 15
RUN apk update && \
    apk --upgrade add postgresql${NEWVERSION} postgresql${NEWVERSION}-client && \
    mkdir -p /var/lib/postgresql/${NEWVERSION}/data
# installing latest PostgreSQL 12
RUN apk update && \
    apk --upgrade add postgresql${OLDVERSION} postgresql${OLDVERSION}-client && \
    mkdir -p /var/lib/postgresql/${OLDVERSION}/data

COPY init.sh /
WORKDIR /tmp
CMD ["/bin/ash", "-c", "/init.sh"]


